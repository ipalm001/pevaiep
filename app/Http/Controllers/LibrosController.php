<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Libros;

class LibrosController extends Controller
{
    public function index()
    {
        $libros=Libros::orderBy('id','DESC')->paginate(10);

        return view('libro.index',
            compact('libros'));
    }
    public function create()
    {
        //dd('create');
        return view('Libro.create');
    }

    public function store(Request $request)
    {
        //dd('store');
        $this->validate($request,[
            'nombre'=>'required',
            'resumen'=>'required',
            'total_paginas'=>'required',
            'fecha_edicion'=>'required',
            'autor'=>'required',
            'precio'=>'required']);

        Libros::create($request->all());
        return redirect()->route('libro.index')->with('success','Registro creado satisfactoriamente');
    }
    public function show($id)
    {
        //dd('store');
        $libros=Libros::find($id);
        return  view('libro.index',compact('libros'));
    }

    public function edit($id)
    {
        //dd('edit');
        $libro=libros::find($id);
        return view('libro.edit',compact('libro'));
    }

    public function update(Request $request, $id)    {
        //dd('update');
        $this->validate($request,[
            'nombre'=>'required',
            'resumen'=>'required',
            'total_paginas'=>'required',
            'fecha_edicion'=>'required',
            'autor'=>'required',
            'precio'=>'required']);

        libros::find($id)->update($request->all());
        return redirect()->route('libro.index')->with('success','Registro actualizado satisfactoriamente');

    }

    public function destroy($id)
    {
        //dd('destroy ',$id);
        Libros::find($id)->delete();
        return redirect()->route('libro.index')->with('success','Registro eliminado satisfactoriamente');
    }

    public function getApiLibros(){
        $libros=Libros::all();
        return response()->json($libros);
    }
}
